/*
 * Name: vulnerableUUT.c
 * Desc: Integer safety exploration for 64-bit Linux OS on x86_64 Intel systems.
 * Author: Reuben Johnston, reub@jhu.edu
 * Course: JHU-ISI, Software Vulnerability Analysis, EN.650.660
 * References:
 *   * 2013, Seacord, Secure Coding in C and C++, Second Edition
 * Notes:
 *   * None
 * Usage:
 *   $ ./vulnerableUUT
 * Copyright:
 *   Reuben Johnston, www.reubenjohnston.com
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

int intOverflow(void) {
	int i;
	i = INT_MAX; // 2,147,483,647
	printf("i = %d\n", i); /* i = 2,147,483,647 */
	i++;
	printf("i = %d\n", i); /* i = -2,147,483,648 */

	return(EXIT_SUCCESS);
}

int intUnderflow(void) {
	int i;
	i = INT_MIN; // -2,147,483,648;
	printf("i = %d\n", i); /* i = -2,147,483,648 */
	i--;
	printf("i = %d\n", i); /* i = 2,147,483,647 */

	return(EXIT_SUCCESS);
}

int intConversion(void) {
	//unsigned, loss of precision
	unsigned int ui = 300;
	printf("ui = %u\n", ui);
	unsigned char uc = ui;
	printf("uc = %hhu\n", uc);

	//unsigned to signed
	unsigned long int ul = ULONG_MAX;
	printf("ul = %lu\n", ul);
	signed char sc = (signed char)ul; /* cast eliminates warning */
	printf("sc = %hhu\n", sc);

	//signed, loss of precision
	signed long int sl = LONG_MAX;
	printf("sl = %ld\n", sl);
	sc = (signed char)sl; /* cast eliminates warning */
	printf("sc = %hhd\n", sc);

	//signed to unsigned
	ui = ULONG_MAX;
	printf("ui = %lu\n", ui);
	signed char c = -1;
	printf("c = %hhd\n", c);
	if (c == ui) {
	  puts("Why is -1 = 4,294,967,295???");
	}

	return(EXIT_SUCCESS);
}

int intTruncation(void) {
	//example 1
	unsigned char sum, c1, c2;
	c1 = 200;
	c2 = 90;
	sum = c1 + c2;
	printf("%d+%d=%hhu\n",c1,c2,sum);

	//example 2
	signed int  si = SCHAR_MAX + 1;
	printf("si=%d\n",si);
	signed char sc = si;
	printf("sc=%hhd\n",sc);

	return(EXIT_SUCCESS);
}

int *table = NULL;
// integer logic
int insert_in_table(int pos, int value) {
	if (!table) {
		table = (int *)malloc(sizeof(int) * 100);
	}
	table[pos] = value;

	return(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
	int status=0;

	status=intOverflow();
	status=intUnderflow();
	status=intConversion();
	status=intTruncation();
	status=insert_in_table(0,100);
	//view the address of table in memory (heap)
	status=insert_in_table(1,100);
	status=insert_in_table(-1,100);

	return(status);
}
